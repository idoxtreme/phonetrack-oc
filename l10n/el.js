OC.L10N.register(
    "phonetrack",
    {
    "PhoneTrack" : "PhoneTrack",
    "Geofencing alert" : "Ειδοποίηση Περιφραγμένης Περιοχής",
    "In session \"%s\", device \"%s\" entered geofencing zone \"%s\"." : "Στη συνεδρία \"%s\", η συσκευή \"%s\" εισήλθε σε περιφραγμένη γεωγραφική περιοχή \"%s\".",
    "left" : "αριστερά",
    "right" : "δεξιά",
    "Show lines" : "Εμφάνιση γραμμών",
    "Hide lines" : "Απόκρυψη Γραμμών",
    "Activate automatic zoom" : "Ενεργοποίηση Αυτόματου Zoom",
    "Disable automatic zoom" : "Απενεργοποίηση Αυτόματου Zoom"
},
"nplurals=2; plural=(n != 1);\nX-Generator: crowdin.com\nX-Crowdin-Project: phonetrack\nX-Crowdin-Language: el");
